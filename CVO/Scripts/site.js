﻿//Run this methods when page is loaded
$(document).ready(function () {

    //Get previous page
    var previousPage = document.referrer;

    //If previous page is loginpage, hide left-arrow in admin
    if (previousPage.indexOf("Login") > 0 || previousPage.indexOf("login") > 0) {
        $(".historybackbutton").hide();
    }

    //If previous page is emtpy, hide left-arrow in admin
    //Previous page is empty when previous page is another website
    if (!previousPage) {
        $(".historybackbutton").hide();
    }

    //Call method for focus on login button
    focusOnLoginButton();

    hidePasswordRequirements();

    //Call method for password strength
    passwordField();
})

//Boolean which set nav to open or closed
//Default on false
var navIsOpen = false;

//Check for closing tab when there's unsaved data in the form
//Default on false
var unsavedDataInForm = false;

var setNav = function () {
    if (navIsOpen) {
        //Close sidebar left
        document.getElementById("mySidenav").style.width = "0px";
        $(".moveToRight").css('margin-left', '0px');
        //Enable horizontal scrolling
        $("body").css('overflow', 'auto');
        navIsOpen = false;
    } else {
        //Open sidebar left
        document.getElementById("mySidenav").style.width = "250px";
        $(".moveToRight").css('margin-left', '250px');
        //Disable horizontal scrolling
        $("body").css('overflow', 'hidden');
        navIsOpen = true;
    }
}


//Check if there's unsaved data in the form
//If unsaved data is in the from, set unsavedDataInForm to true
$(":input").change(function () {
    unsavedDataInForm = true;
});

//Check if user closes the page
var unloadPage = function () {

    //Check if action not is login
    if (window.location.pathname.indexOf("Login") <= 0 && window.location.pathname.indexOf("login") <= 0) {
        //If unsavedDataInForm is true, show alert box which says there's unsaved data
        if (unsavedDataInForm) {
            return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
        }
    }
}

//Call function: unloadPage when user closes the page
window.onbeforeunload = unloadPage;

//Extra check for unsaved data
//If form is submitted, set unsavedDataInForm to false, won't pop up
$("form").submit(function () {
    unsavedDataInForm = false;
});

//When loginpage is active, auto focus on login button if page is loaded
var focusOnLoginButton = function () {
    if (window.location.pathname.indexOf("Login") > 0) {
        $("#LoginButton").focus();
    }
}

//Method for showing password strenght bar
function passwordStrength(password) {

    var desc = [{ 'width': '0px' }, { 'width': '20%' }, { 'width': '40%' }, { 'width': '60%' }, { 'width': '80%' }, { 'width': '100%' }];

    var descClass = ['', 'progress-bar-danger', 'progress-bar-danger', 'progress-bar-warning', 'progress-bar-success', 'progress-bar-success'];

    var score = 0;

    //if password bigger than 6 give 1 point
    if (password.length > 6) score++;

    //if password has both lower and uppercase characters give 1 point	
    if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) score++;

    //if password has at least one number give 1 point
    if (password.match(/[0-9]/)) score++;

    //if password has at least one special caracther give 1 point
    if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) score++;

    //if password bigger than 12 give another 1 point
    if (password.length > 10) score++;

    // display indicator
    $("#passwordStrength").removeClass(descClass[score - 1]).addClass(descClass[score]).css(desc[score]);
}

//Method for password field
//When user is typing in password field, show bar for strength of password
var passwordField = function () {
    $("#ChangePassword_NewPassword, #NewUser_Password").focus(function () {
        showPasswordRequirements();
    });
    $("#ChangePassword_NewPassword, #NewUser_Password").focusout(function () {
        hidePasswordRequirements();
    });
    $("#ChangePassword_NewPassword, #NewUser_Password").keyup(function () {
        passwordStrength(jQuery(this).val());
        passwordRequirementCheck(jQuery(this).val());
    });
}

var showPasswordRequirements = function () {
    $(".progress, .passwordRequirements").show();
}

var hidePasswordRequirements = function () {
    $(".progress, .passwordRequirements").hide();
}

//Method for password requirements
var passwordRequirementCheck = function (password) {

    score = 0;

    //check if password has at least 8 characters
    if (password.length > 8) {
        setTextColor("#pwMinCharacters", "green");
        score++;
    } else {
        setTextColor("#pwMinCharacters", "red");
        score--;
    }

    //check if password has at least one special character
    if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) {
        setTextColor("#pwAttribute", "green");
        score++;
    } else {
        setTextColor("#pwAttribute", "red");
        score--;
    }

    //check if password has at least one number
    if (password.match(/[0-9]/)) {
        setTextColor("#pwNumber", "green");
        score++;
    } else {
        setTextColor("#pwNumber", "red");
        score--;
    }

    //check if password has at least one capital letter
    if (password.match(/[A-Z]/)) {
        setTextColor("#pwCapitalLetter", "green");
        score++;
    } else {
        setTextColor("#pwCapitalLetter", "red");
        score--;
    }

    //if score is 4, enable button for change password
    if (score == 4) {
        $("#passwordButton").prop('disabled', false);
    } else {
        $("#passwordButton").prop('disabled', true);
    }
}

//Method for changing textcolor
var setTextColor = function (id, color) {
    $(id).css('color', color);
}