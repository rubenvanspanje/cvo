﻿using CVO.DAL;
using CVO.Models;
using PagedList;
using System.Collections.Generic;

namespace CVO.ViewModels
{
    public class IndexVM
    {
        private CvoDbContext db = new CvoDbContext();

        //News
        public List<Nieuws> NieuwsList { get; set; }
        public PagedList<Nieuws> NieuwsPagedList { get; set; }
        public Nieuws Nieuws { get; set; }

        //Sponsor
        public List<Sponsor> SponsorList { get; set; }
        public PagedList<Sponsor> SponsorPagedList { get; set; }

        //Calendar
        public List<Kalender> KalenderList { get; set; }
        public PagedList<Kalender> KalenderPagedList { get; set; }
        public Kalender Kalender { get; set; }

        //Team
        public List<Team> TeamList { get; set; }
        public Team Team { get; set; }
        public PagedList<Team> TeamPagedList { get; set; }

        //Contact
        public Locatie Locatie { get; set; }
        public ContactForm contactform { get; set; }

        //Album
        public List<Album> AlbumList { get; set; }
        public PagedList<Album> AlbumPagedList { get; set; }

        //Member
        public List<Bestuurslid> BestuurslidList { get; set; }
        public List<Bestuurslid> CalamiteitList { get; set; }

        //Searching
        public string Search { get; set; }

        public IndexVM()
        {
            Locatie = db.Locatie.Find(1);
        }
    }
}