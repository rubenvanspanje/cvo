﻿using CVO.DAL;
using CVO.Models;
using System.Collections.Generic;
using System.Linq;

namespace CVO.ViewModels
{
    public class AdminVM
    {
        private CvoDbContext db = new CvoDbContext();

        //User
        public Gebruiker Gebruiker { get; set; }
        public List<Gebruiker> GebruikerList { get; set; }
        public Gebruiker CurrentUser { get; set; }
        public NewUser NewUser { get; set; }
        public ChangePassword ChangePassword { get; set; }

        //Calendar
        public Kalender Kalender { get; set; }
        public List<Kalender> KalenderList { get; set; }

        //Sponsor
        public Sponsor Sponsor { get; set; }
        public List<Sponsor> SponsorList { get; set; }

        //Location
        public Locatie Locatie { get; set; }

        //News
        public Nieuws Nieuws { get; set; }
        public List<Nieuws> NieuwsList { get; set; }

        //Albums
        public Album Album { get; set; }
        public List<Album> AlbumList { get; set; }

        //Teams
        public Team Team { get; set; }
        public List<Team> TeamList { get; set; }

        //Level
        public List<Level> LevelList { get; set; }
        public Level Level { get; set; }

        //Member
        public List<Bestuurslid> BestuurslidList { get; set; }
        public Bestuurslid Bestuurslid { get; set; }
        public List<BestuurslidCategorie> BestuurslidCategorieList { get; set; }
        public BestuurslidCategorie BestuurslidCategorie { get; set; }

        //Levels for security
        public int LevelAlbum { get; set; }
        public int LevelUser { get; set; }
        public int LevelCalendar { get; set; }
        public int LevelLocation { get; set; }
        public int LevelNews { get; set; }
        public int LevelSponsor { get; set; }
        public int LevelTeam { get; set; }


        //Constructor
        public AdminVM()
        {
            //Set currentUser to logged in user
            CurrentUser = Gebruiker.ReturnCurrentUser();

            LevelAlbum = ReturnMinLevel(1);
            LevelUser = ReturnMinLevel(4);
            LevelCalendar = ReturnMinLevel(6);
            LevelLocation = ReturnMinLevel(5);
            LevelNews = ReturnMinLevel(7);
            LevelSponsor = ReturnMinLevel(8);
            LevelTeam = ReturnMinLevel(9);
        }

        public int ReturnMinLevel(int category)
        {
            return db.Levels.Find(category).MinLevel;
        }
    }
}