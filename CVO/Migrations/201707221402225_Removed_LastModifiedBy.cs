namespace CVO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Removed_LastModifiedBy : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Levels", "User", "dbo.Gebruikers");
            DropIndex("dbo.Levels", new[] { "User" });
            DropColumn("dbo.Levels", "User");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Levels", "User", c => c.Int(nullable: false));
            CreateIndex("dbo.Levels", "User");
            AddForeignKey("dbo.Levels", "User", "dbo.Gebruikers", "Id", cascadeDelete: true);
        }
    }
}
