namespace CVO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gebruikers", "IsBlocked", c => c.Boolean(nullable: false));
            AddColumn("dbo.Gebruikers", "LastModifiedAt", c => c.DateTime(nullable: false));
            AddColumn("dbo.Gebruikers", "CreatedAt", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gebruikers", "CreatedAt");
            DropColumn("dbo.Gebruikers", "LastModifiedAt");
            DropColumn("dbo.Gebruikers", "IsBlocked");
        }
    }
}
