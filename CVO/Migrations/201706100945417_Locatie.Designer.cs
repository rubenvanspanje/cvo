// <auto-generated />
namespace CVO.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Locatie : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Locatie));
        
        string IMigrationMetadata.Id
        {
            get { return "201706100945417_Locatie"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
