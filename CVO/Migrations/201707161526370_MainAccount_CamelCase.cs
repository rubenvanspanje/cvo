namespace CVO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MainAccount_CamelCase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gebruikers", "MainAccount", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gebruikers", "MainAccount");
        }
    }
}
