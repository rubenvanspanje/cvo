namespace CVO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Levels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category = c.String(nullable: false),
                        MinLevel = c.Int(nullable: false),
                        LastModifiedAt = c.DateTime(nullable: false),
                        User = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gebruikers", t => t.User, cascadeDelete: true)
                .Index(t => t.User);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Levels", "User", "dbo.Gebruikers");
            DropIndex("dbo.Levels", new[] { "User" });
            DropTable("dbo.Levels");
        }
    }
}
