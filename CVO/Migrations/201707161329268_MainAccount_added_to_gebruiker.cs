namespace CVO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MainAccount_added_to_gebruiker : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gebruikers", "mainAccount", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gebruikers", "mainAccount");
        }
    }
}
