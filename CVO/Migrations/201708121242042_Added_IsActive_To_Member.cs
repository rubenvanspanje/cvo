namespace CVO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_IsActive_To_Member : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bestuurslids", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bestuurslids", "IsActive");
        }
    }
}
