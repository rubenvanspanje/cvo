namespace CVO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Delete_mainAccount : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Gebruikers", "mainAccount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Gebruikers", "mainAccount", c => c.Boolean(nullable: false));
        }
    }
}
