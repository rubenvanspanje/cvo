namespace CVO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Member : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bestuurslids",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Function = c.String(nullable: false),
                        EmailAddress = c.String(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        LastModifiedAt = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BestuurslidCategories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.BestuurslidCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        IsCalamity = c.Boolean(nullable: false),
                        LastModifiedAt = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bestuurslids", "CategoryId", "dbo.BestuurslidCategories");
            DropIndex("dbo.Bestuurslids", new[] { "CategoryId" });
            DropTable("dbo.BestuurslidCategories");
            DropTable("dbo.Bestuurslids");
        }
    }
}
