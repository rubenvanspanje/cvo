namespace CVO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Locatie : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Locaties",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Stadium = c.String(),
                        Street = c.String(nullable: false),
                        Number = c.String(nullable: false),
                        ZipCode = c.String(nullable: false),
                        City = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Locaties");
        }
    }
}
