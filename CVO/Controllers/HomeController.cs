﻿using System;
using System.Linq;
using System.Web.Mvc;
using CVO.DAL;
using PagedList;
using CVO.ViewModels;
using CVO.Models;
using System.Data;
using System.Data.Entity;

namespace CVO.Controllers
{
    public class HomeController : Controller
    {

        private CvoDbContext db = new CvoDbContext();
        private IndexVM indexVM;

        public HomeController()
        {
            indexVM = new IndexVM();
        }

        public ActionResult Index(int? page)
        {
            //Get all news from database
            //Orderby locked DESC, DateCreated DESC
            //Where active is true
            var news = from n in db.Nieuws
                       where n.Active == true
                       orderby n.Locked descending, n.DateCreated descending
                       select n;

            //Pagination for news (3 per page)
            int pageSize = 3;
            int pageNumber = (page ?? 1);

            //Get all sponsors from database
            //Orderby dateCreated ASC
            //Where active is true
            //Take first 4
            var sponsors = (from s in db.Sponsors
                            where s.Active == true
                            orderby s.DateCreated ascending
                            select s).Take(4);

            //Set dateTime to DateTime.Now with 0 hours, 0 minutes and 0 seconds
            var dateNow = DateTime.Now;
            var dateTime = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, 0, 0, 0);

            //Get all calendar items from database
            //Where startDate or endDate are in the future
            //Where active is true
            //Orderby dateStart ASC
            //Take first 4
            var calendar = (from k in db.Kalender
                            where dateTime <= k.DateStart || dateTime <= k.DateEnd
                            where k.Active == true
                            orderby k.DateStart ascending
                            select k).Take(4);

            //Get all albums from database
            //Where Active is true
            //Order by dateCreated DESC
            //Take first 4
            var albums = (from a in db.Albums
                          where a.Active == true
                          orderby a.DateCreated descending
                          select a).Take(4);

            indexVM.NieuwsPagedList = new PagedList<Nieuws>(news, pageNumber, pageSize);
            indexVM.SponsorList = sponsors.ToList();
            indexVM.KalenderList = calendar.ToList();
            indexVM.AlbumList = albums.ToList();

            return View(indexVM);
        }

        public ActionResult Teams(int? page)
        {
            //Get all teams from database
            //Orderby name ascending
            //Where active is true
            var teams = from t in db.Teams
                        where t.Active == true
                        orderby t.Name ascending
                        select t;

            //Pagination for teams (12 per page)
            int pageSize = 12;
            int pageNumber = (page ?? 1);

            indexVM.TeamPagedList = new PagedList<Team>(teams, pageNumber, pageSize);
            return View(indexVM);
        }

        public ActionResult Team(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Teams");
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return RedirectToAction("Teams");
            }

            indexVM.Team = team;

            return View(indexVM);
        }

        public ActionResult Nieuws(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Nieuws nieuws = db.Nieuws.Find(id);
            if (nieuws == null)
            {
                return RedirectToAction("Index");
            }

            indexVM.Nieuws = nieuws;

            return View(indexVM);
        }

        public ActionResult Kalender()
        {

            //Set dateTime to DateTime.Now with 0 hours, 0 minutes and 0 seconds
            var dateNow = DateTime.Now;
            var dateTime = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, 0, 0, 0);

            //Get all calendar items from database
            //Where startDate or endDate are in the future
            //Where active is true
            //Orderby dateStart ASC
            var calendar = from k in db.Kalender
                           where dateTime <= k.DateStart || dateTime <= k.DateEnd
                           where k.Active == true
                           orderby k.DateStart ascending
                           select k;

            indexVM.KalenderList = calendar.ToList();

            return View(indexVM);
        }

        public ActionResult KalenderItem(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Kalender");
            }
            Kalender kalenderItem = db.Kalender.Find(id);
            if (kalenderItem == null)
            {
                return RedirectToAction("Kalender");
            }

            indexVM.Kalender = kalenderItem;

            return View(indexVM);
        }

        public ActionResult Albums(int? page)
        {

            //Get all albums from database
            //Orderby dateCreated descending
            //Where active is true
            var albums = from a in db.Albums
                         where a.Active == true
                         orderby a.DateCreated descending
                         select a;

            //Pagination for albums (12 per page)
            int pageSize = 12;
            int pageNumber = (page ?? 1);

            indexVM.AlbumPagedList = new PagedList<Album>(albums, pageNumber, pageSize);
            return View(indexVM);
        }

        public ActionResult Sponsors()
        {
            //Get all sponsors from database
            //Orderby dateCreated ASC
            //Where active is true
            var sponsors = from s in db.Sponsors
                           where s.Active == true
                           orderby s.DateCreated ascending
                           select s;

            indexVM.SponsorList = sponsors.ToList();

            return View(indexVM);
        }

        public ActionResult Contact()
        {

            var bestuurslid = db.Bestuurslid.Include(b => b.Category);

            //Get all members from database
            //Orderby category ASC
            //Where active is true
            var bestuurslidList = from b in bestuurslid
                                  where b.IsActive == true
                                  where b.Category.IsCalamity == false
                                  orderby b.Category.Title ascending
                                  select b;

            var calamiteitList = from c in bestuurslid
                                 where c.IsActive == true
                                 where c.Category.IsCalamity == true
                                 orderby c.Category.Title ascending
                                 select c;


            indexVM.BestuurslidList = bestuurslidList.ToList();
            indexVM.CalamiteitList = calamiteitList.ToList();

            return View(indexVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact([Bind(Include = "Name,Phone,Email,Message")] ContactForm contactform)
        {
            if (ModelState.IsValid)
            {
                //RubenTODO: mail versturen
                return View(indexVM);
            }

            return View(indexVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(string search)
        {
            return RedirectToAction("Zoeken", new { search = search });
        }

        public ActionResult Zoeken(string search, int? pageNieuws, int? pageTeam, int? pageKalender, int? pageAlbum)
        {
            if (String.IsNullOrEmpty(search))
            {
                return RedirectToAction("Index");
            }

            var news = from n in db.Nieuws
                       where n.Active == true
                       where n.Title.Contains(search) ||
                       n.Content.Contains(search)
                       orderby n.Locked descending, n.DateCreated descending
                       select n;

            var albums = from a in db.Albums
                         where a.Active == true
                         where a.Title.Contains(search)
                         orderby a.DateCreated descending
                         select a;

            var calendar = from c in db.Kalender
                           where c.Active == true
                           where c.Description.Contains(search) ||
                           c.Location.Contains(search) ||
                           c.Title.Contains(search)
                           orderby c.DateStart ascending
                           select c;

            var teams = from t in db.Teams
                        where t.Active == true
                        where t.Description.Contains(search) ||
                        t.SponsorName.Contains(search) ||
                        t.Name.Contains(search)
                        orderby t.Name ascending
                        select t;

            int pageSize = 3;

            int pageNumberTeam = (pageTeam ?? 1);
            int pageNumberAlbum = (pageAlbum ?? 1);
            int pageNumberKalender = (pageKalender ?? 1);
            int pageNumberNieuws = (pageNieuws ?? 1);

            indexVM.NieuwsPagedList = new PagedList<Nieuws>(news, pageNumberNieuws, pageSize);
            indexVM.AlbumPagedList = new PagedList<Album>(albums, pageNumberAlbum, pageSize);
            indexVM.KalenderPagedList = new PagedList<Kalender>(calendar, pageNumberKalender, pageSize);
            indexVM.TeamPagedList = new PagedList<Team>(teams, pageNumberTeam, pageSize);
            indexVM.Search = search;

            return View(indexVM);
        }
    }
}