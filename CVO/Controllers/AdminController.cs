﻿using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;
using System.Web.Mvc;

namespace CVO.Controllers
{
    public class AdminController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private AdminVM adminVM;

        public AdminController()
        {
            isLoggedIn = false;

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                adminVM = new AdminVM();
            }
        }

        // GET: Admin/Index
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction("Login");
            }

            return View(adminVM);
        }

        // GET: Admin/Login
        public ActionResult Login()
        {
            if (isLoggedIn)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            Gebruiker gebruiker = new Gebruiker();

            return View(gebruiker);
        }

        // POST: Admin/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Gebruiker gebruiker)
        {
            bool emailExists = false;
            Gebruiker dbGebruiker = null;

            //Loop through all users in database
            foreach (var user in db.Gebruikers)
            {
                //When email exists in database
                //Set emailExists to true and continue with user
                //Exit the loop
                if (user.Email == gebruiker.Email)
                {
                    emailExists = true;
                    dbGebruiker = user;
                    break;
                }
            }

            //If email exists in database
            if (emailExists)
            {

                if (!dbGebruiker.IsBlocked)
                {
                    //Call gebruiker method to encrypt password
                    string encryptedPassword = Gebruiker.EncryptPassword(gebruiker.Password);

                    //Check if database password is the same as inserted password (encrypted passwords)
                    if (dbGebruiker.Password == encryptedPassword)
                    {
                        //Create session with userId and level
                        //Redirect to dashboard page
                        Session["userId"] = dbGebruiker.Id.ToString();
                        Session["userLevel"] = dbGebruiker.Level.ToString();
                        return RedirectToAction("Index");

                    }
                    else
                    {
                        ViewBag.errorMessage = "Email of wachtwoord is onjuist";
                        return View();
                    }
                }
                else
                {
                    ViewBag.errorMessage = "Gebruiker is geblokkeerd";
                    return View();
                }
            }
            else
            {
                ViewBag.errorMessage = "Email of wachtwoord is onjuist";
                return View();
            }
        }

        // GET: Admin/Logout
        public ActionResult Logout()
        {
            //Abondon all sessions
            //Redirect to login page
            Session.Abandon();
            return RedirectToAction("Login");
        }

        // GET: Admin/ForgotPassword
        public ActionResult ForgotPassword()
        {
            if (isLoggedIn)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            Gebruiker gebruiker = new Gebruiker();

            return View(gebruiker);
        }

        // POST: Admin/ForgotPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(Gebruiker gebruiker)
        {
            if (gebruiker.Email == null)
            {
                return View(gebruiker);
            }

            bool emailExists = false;
            Gebruiker dbGebruiker = null;

            //Loop through all users in database
            foreach (var user in db.Gebruikers)
            {
                //When email exists in database
                //Set emailExists to true and continue with user
                //Exit the loop
                if (user.Email == gebruiker.Email)
                {
                    emailExists = true;
                    dbGebruiker = user;
                    break;
                }
            }

            //If email exists in database
            //Send mail to user
            if (emailExists)
            {
                //RubenTODO: Mail versturen met tijdelijk wachtwoord en wachtwoord op null zetten
                return RedirectToAction("SendPassword");
            }
            else
            {
                ViewBag.errorMessage = "Email adres komt niet in de database voor";
                return View(gebruiker);
            }
        }

        // GET: Admin/SendPassword
        public ActionResult SendPassword()
        {
            if (isLoggedIn)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            return View();
        }
    }
}
