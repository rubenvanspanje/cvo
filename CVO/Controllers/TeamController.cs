﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using System;
using CVO.ViewModels;

namespace CVO.Controllers
{
    public class TeamController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public TeamController()
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var teamLevel = db.Levels.Find(9).MinLevel;

                //Check if page level not is higher than user level
                if (teamLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: Team
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            var teams = from t in db.Teams
                        orderby t.Name ascending
                        select t;

            adminVM.TeamList = teams.ToList();
            return View(adminVM);
        }

        // GET: Team/Details/5
        public ActionResult Details(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // GET: Team/Create
        public ActionResult Create()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Create object of Team so default values can be set
            Team team = new Team();

            //Set active to true, so active checkbox is checked by default
            team.Active = true;

            //Set DateCreated to DateTime.Now
            team.DateCreated = DateTime.Now;

            adminVM.Team = team;
            return View(adminVM);
        }

        // POST: Team/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Image,Description,Active,SponsorName,SponsorWebsite,SponsorImage,DateCreated")] Team team)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (team.Image != null)
            {
                //Save image to server and change name to imagepath
                team.Image = Image.SaveImageToServer();
            }

            if (ModelState.IsValid)
            {
                db.Teams.Add(team);
                db.SaveChanges();
                return RedirectToAction("AddSponsor", new { id = team.Id });
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // GET: Team/AddSponsor
        public ActionResult AddSponsor(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // POST: Team/AddSponsor
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSponsor([Bind(Include = "Id,Name,Image,Description,Active,SponsorName,SponsorWebsite,SponsorImage,DateCreated")] Team team)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (team.SponsorWebsite != null)
            {
                if (!team.SponsorWebsite.Contains("http://") && !team.SponsorWebsite.Contains("https://"))
                {
                    team.SponsorWebsite = "http://" + team.SponsorWebsite;
                }
            }

            if (team.SponsorImage != null)
            {
                //Save image to server and change name to imagepath
                team.SponsorImage = Image.SaveImageToServer();
            }

            if (ModelState.IsValid)
            {
                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // GET: Team/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // POST: Team/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Image,Description,Active,SponsorName,SponsorWebsite,SponsorImage,DateCreated")] Team team)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (team.SponsorWebsite != null)
            {
                if (!team.SponsorWebsite.Contains("http://") && !team.SponsorWebsite.Contains("https://"))
                {
                    team.SponsorWebsite = "http://" + team.SponsorWebsite;
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // GET: Team/EditImage/5
        public ActionResult EditImage(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // POST: Team/EditImage/5       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditImage([Bind(Include = "Id,Name,Image,Description,Active,SponsorName,SponsorWebsite,SponsorImage,DateCreated")] Team team)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (team.Image != null)
            {
                //Save image to server and change name to imagepath
                team.Image = Image.SaveImageToServer();
            }
            else
            {
                return RedirectToAction("Edit", new { id = team.Id });
            }
            if (ModelState.IsValid)
            {
                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = team.Id });
            }
            return RedirectToAction("Edit", new { id = team.Id });
        }

        // GET: Team/EditSponsorImage/5
        public ActionResult EditSponsorImage(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // POST: Team/EditSponsorImage/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSponsorImage([Bind(Include = "Id,Name,Image,Description,Active,SponsorName,SponsorWebsite,SponsorImage,DateCreated")] Team team)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (team.SponsorImage != null)
            {
                //Save image to server and change name to imagepath
                team.SponsorImage = Image.SaveImageToServer();
            }
            else
            {
                return RedirectToAction("Edit", new { id = team.Id });
            }
            if (ModelState.IsValid)
            {
                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = team.Id });
            }
            return RedirectToAction("Edit", new { id = team.Id });
        }

        // POST: Team/DeleteSponsorImage/5
        [HttpPost, ActionName("DeleteSponsorImage")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteSponsorImage([Bind(Include = "Id,Name,Image,Description,Active,SponsorName,SponsorWebsite,SponsorImage,DateCreated")] Team team)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (team.SponsorImage != null)
            {
                //Delete image
                Image.DeleteImageFromServer(team.SponsorImage);

                //Set image to null in database
                team.SponsorImage = null;
            }

            if (ModelState.IsValid)
            {
                db.Entry(team).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // GET: Team/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Team team = db.Teams.Find(id);
            if (team == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Team = team;
            return View(adminVM);
        }

        // POST: Team/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            Team team = db.Teams.Find(id);

            //If image is set, delete image from server
            if (team.Image != null)
            {
                //Delete image
                Image.DeleteImageFromServer(team.Image);
            }

            //If sponsorImage is set, delete image from server
            if (team.SponsorImage != null)
            {
                //Delete image
                Image.DeleteImageFromServer(team.SponsorImage);
            }

            db.Teams.Remove(team);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
