﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;

namespace CVO.Controllers
{

    public class GebruikerController : Controller
    {

        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public GebruikerController()
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var userLevel = db.Levels.Find(4).MinLevel;

                //Check if page level not is higher than user level
                if (userLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: Gebruiker/Create
        public ActionResult Create()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Create object of gebruiker
            NewUser newUser = new NewUser();

            adminVM.NewUser = newUser;
            return View(adminVM);
        }

        // POST: Gebruiker/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FirstName,LastName,Email,Password,PasswordRepeat,Level")] NewUser newUser)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Set bool emailexits to false by default
            bool emailExists = false;

            //Check if form input is valid
            if (ModelState.IsValid)
            {
                //Loop through all users in database
                foreach (var user in db.Gebruikers)
                {
                    //When email exists in database
                    //Set emailExists to true
                    //Exit the loop
                    if (user.Email == newUser.Email)
                    {
                        emailExists = true;
                        break;
                    }
                }

                //If email already exists, return view
                if (emailExists)
                {
                    //Show error message
                    ViewBag.errorMessage = "Emailadres bestaat al";
                    adminVM.NewUser = newUser;
                    return View(adminVM);
                }

                var errorMessage = newUser.ChechIfPasswordMeetsRequirements(newUser);

                if (errorMessage == null)
                {
                    string encryptedPassword = Gebruiker.EncryptPassword(newUser.Password);

                    //Create user of newUser
                    Gebruiker gebruiker = new Gebruiker();
                    gebruiker.Email = newUser.Email;
                    gebruiker.FirstName = newUser.FirstName;
                    gebruiker.LastName = newUser.LastName;
                    gebruiker.Password = encryptedPassword;
                    gebruiker.Level = newUser.Level;
                    gebruiker.Image = null;
                    gebruiker.IsBlocked = false;
                    gebruiker.LastModifiedAt = DateTime.Now;
                    gebruiker.CreatedAt = DateTime.Now;
                    gebruiker.MainAccount = false;

                    //RubenTODO: Send confirmationMail to user

                    //Add user to database
                    db.Gebruikers.Add(gebruiker);
                    db.SaveChanges();

                    //Redirect to CreateImage
                    return RedirectToAction("CreateImage", new { id = gebruiker.Id });
                }
                else
                {
                    ViewBag.errorMessage = errorMessage;
                    return View(adminVM);
                }
            }
            else
            {
                adminVM.NewUser = newUser;
                return View(adminVM);
            }
        }

        // GET: Gebruiker/CreateImage/5
        public ActionResult CreateImage(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }

            Gebruiker gebruiker = db.Gebruikers.Find(id);

            if (gebruiker == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Gebruiker = gebruiker;

            return View(adminVM);
        }

        // POST: Gebruiker/EditImage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateImage([Bind(Include = "Id,FirstName,LastName,Email,Password,Image,Level,IsBlocked,LastModifiedAt,CreatedAt,MainAccount")] Gebruiker gebruiker)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Check if image is set
            if (gebruiker.Image != null)
            {
                //Save image to server and change name to imagepath
                gebruiker.Image = Image.SaveImageToServer();
            }
            else
            {
                ViewBag.errorMessage = "Er is geen foto geselecteerd";

                adminVM.Gebruiker = gebruiker;
                return View(adminVM);
            }

            if (ModelState.IsValid)
            {
                gebruiker.LastModifiedAt = DateTime.Now;
                db.Entry(gebruiker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        // GET: Gebruiker/Index
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Get all users, ordered by id desc
            var users = from a in db.Gebruikers
                        orderby a.Id descending
                        select a;

            adminVM.GebruikerList = users.ToList();

            return View(adminVM);
        }

        // GET: Gebruiker/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }

            Gebruiker gebruiker = db.Gebruikers.Find(id);

            if (gebruiker == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Gebruiker = gebruiker;

            return View(adminVM);
        }

        // POST: Gebruiker/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Email,Image,Password,Level,IsBlocked,LastModifiedAt,CreatedAt,MainAccount")] Gebruiker gebruiker)
        {

            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //RubenTODO: Controleren of email al bestaat in de database

            if (ModelState.IsValid)
            {
                gebruiker.LastModifiedAt = DateTime.Now;
                //Save changes in database
                db.Entry(gebruiker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        // GET: Gebruiker/Details/5
        public ActionResult Details(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Gebruiker gebruiker = db.Gebruikers.Find(id);
            if (gebruiker == null)
            {
                return HttpNotFound();
            }

            adminVM.Gebruiker = gebruiker;

            return View(adminVM);
        }

        // GET: Gebruiker/Block/5
        public ActionResult Block(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }

            Gebruiker gebruiker = db.Gebruikers.Find(id);

            if (gebruiker == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Gebruiker = gebruiker;

            return View(adminVM);
        }

        // POST: Gebruiker/Block
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Block([Bind(Include = "Id,FirstName,LastName,Email,Image,Password,Level,IsBlocked,LastModifiedAt,CreatedAt,MainAccount")] Gebruiker gebruiker)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            Gebruiker currentUser = Gebruiker.ReturnCurrentUser();

            //Check if blocked user not is the MainAccount or own account
            if (gebruiker.MainAccount || gebruiker.Id == currentUser.Id)
            {
                return RedirectToAction("Index");
            }

            //If user is blocked, unblock user 
            //and if user is not blocked, block user
            if (gebruiker.IsBlocked)
            {
                gebruiker.IsBlocked = false;
            }
            else
            {
                gebruiker.IsBlocked = true;
            }

            if (ModelState.IsValid)
            {
                gebruiker.LastModifiedAt = DateTime.Now;
                db.Entry(gebruiker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            adminVM.Gebruiker = gebruiker;
            return View(adminVM);
        }

        // GET: Gebruiker/EditImage/5
        public ActionResult EditImage(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Gebruiker gebruiker = db.Gebruikers.Find(id);
            if (gebruiker == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Gebruiker = gebruiker;

            return View(adminVM);
        }

        // POST: Gebruiker/EditImage/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditImage([Bind(Include = "Id,FirstName,LastName,Email,Image,Password,Level,IsBlocked,LastModifiedAt,CreatedAt,MainAccount")] Gebruiker gebruiker)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (gebruiker.Image != null)
            {
                //Save image to server and change name to imagepath
                gebruiker.Image = Image.SaveImageToServer();
            }
            else
            {
                return RedirectToAction("Edit", new { id = gebruiker.Id });
            }
            if (ModelState.IsValid)
            {
                gebruiker.LastModifiedAt = DateTime.Now;
                db.Entry(gebruiker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = gebruiker.Id });
            }
            return RedirectToAction("Edit", new { id = gebruiker.Id });
        }

        // POST: Gebruiker/DeleteImage
        [HttpPost, ActionName("DeleteImage")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteImage([Bind(Include = "Id,FirstName,LastName,Email,Image,Password,Level,IsBlocked,LastModifiedAt,CreatedAt,MainAccount")] Gebruiker gebruiker)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (gebruiker.Image != null)
            {
                //Delete image
                Image.DeleteImageFromServer(gebruiker.Image);

                //Set image to null in database
                gebruiker.Image = null;
            }

            if (ModelState.IsValid)
            {
                db.Entry(gebruiker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
    }
}
