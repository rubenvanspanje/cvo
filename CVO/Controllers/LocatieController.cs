﻿using System.Data.Entity;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;

namespace CVO.Controllers
{
    public class LocatieController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public LocatieController()
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var locationLevel = db.Levels.Find(5).MinLevel;

                //Check if page level not is higher than user level
                if (locationLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: Locatie/Index
        // Always id 1 because there is one location
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            var id = 1;
            Locatie locatie = db.Locatie.Find(id);
            if (locatie == null)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            adminVM.Locatie = locatie;

            return View(adminVM);
        }

        // POST: Locatie/Index/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Id,Stadium,Street,Number,ZipCode,City,Phone")] Locatie locatie)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (ModelState.IsValid)
            {
                db.Entry(locatie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(locatie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
