﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;

namespace CVO.Controllers
{
    public class BestuurslidCategorieController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public BestuurslidCategorieController()
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var albumLevel = db.Levels.Find(1).MinLevel;

                //Check if page level not is higher than user level
                if (albumLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: BestuurslidCategorie
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            var bestuurslidCategorie = from b in db.BestuurslidCategorie
                                       orderby b.Title
                                       select b;

            adminVM.BestuurslidCategorieList = bestuurslidCategorie.ToList();

            return View(adminVM);
        }

        // GET: BestuurslidCategorie/Details/5
        public ActionResult Details(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            BestuurslidCategorie bestuurslidCategorie = db.BestuurslidCategorie.Find(id);
            if (bestuurslidCategorie == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.BestuurslidCategorie = bestuurslidCategorie;
            return View(adminVM);
        }

        // GET: BestuurslidCategorie/Create
        public ActionResult Create()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            return View(adminVM);
        }

        // POST: BestuurslidCategorie/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,IsCalamity,LastModifiedAt,CreatedAt")] BestuurslidCategorie bestuurslidCategorie)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (ModelState.IsValid)
            {
                bestuurslidCategorie.LastModifiedAt = DateTime.Now;
                bestuurslidCategorie.CreatedAt = DateTime.Now;
                db.BestuurslidCategorie.Add(bestuurslidCategorie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.BestuurslidCategorie = bestuurslidCategorie;
            return View(adminVM);
        }

        // GET: BestuurslidCategorie/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            BestuurslidCategorie bestuurslidCategorie = db.BestuurslidCategorie.Find(id);
            if (bestuurslidCategorie == null)
            {
                return RedirectToAction("Index");
            }
            adminVM.BestuurslidCategorie = bestuurslidCategorie;
            return View(bestuurslidCategorie);
        }

        // POST: BestuurslidCategorie/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,IsCalamity,LastModifiedAt,CreatedAt")] BestuurslidCategorie bestuurslidCategorie)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (ModelState.IsValid)
            {
                bestuurslidCategorie.LastModifiedAt = DateTime.Now;
                db.Entry(bestuurslidCategorie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.BestuurslidCategorie = bestuurslidCategorie;
            return View(bestuurslidCategorie);
        }

        // GET: BestuurslidCategorie/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            BestuurslidCategorie bestuurslidCategorie = db.BestuurslidCategorie.Find(id);
            if (bestuurslidCategorie == null)
            {
                return RedirectToAction("Index");
            }
            adminVM.BestuurslidCategorie = bestuurslidCategorie;

            return View(adminVM);
        }

        // POST: BestuurslidCategorie/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            BestuurslidCategorie bestuurslidCategorie = db.BestuurslidCategorie.Find(id);
            db.BestuurslidCategorie.Remove(bestuurslidCategorie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
