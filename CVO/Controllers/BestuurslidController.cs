﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;

namespace CVO.Controllers
{
    public class BestuurslidController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public BestuurslidController()
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var albumLevel = db.Levels.Find(1).MinLevel;

                //Check if page level not is higher than user level
                if (albumLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: Bestuurslid
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            var bestuurslid = db.Bestuurslid.Include(b => b.Category);

            //Get all members, ordered by name ASC
            var bestuursleden = from m in bestuurslid
                                orderby m.Name ascending
                                select m;

            adminVM.BestuurslidList = bestuursleden.ToList();
            return View(adminVM);
        }

        // GET: Bestuurslid/Details/5
        public ActionResult Details(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Bestuurslid bestuurslid = db.Bestuurslid.Find(id);
            if (bestuurslid == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Bestuurslid = bestuurslid;
            return View(adminVM);
        }

        // GET: Bestuurslid/Create
        public ActionResult Create()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Create object of bestuurslid
            Bestuurslid bestuurslid = new Bestuurslid();

            //Set active to true, so active checkbox is checked by default
            bestuurslid.IsActive = true;

            adminVM.Bestuurslid = bestuurslid;

            ViewBag.CategoryId = new SelectList(db.BestuurslidCategorie, "Id", "Title");
            return View(adminVM);
        }

        // POST: Bestuurslid/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,IsActive,Function,EmailAddress,PhoneNumber,CategoryId,LastModifiedAt,CreatedAt")] Bestuurslid bestuurslid, int CategoryId)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            bestuurslid.CategoryId = CategoryId;

            if (ModelState.IsValid)
            {
                bestuurslid.LastModifiedAt = DateTime.Now;
                bestuurslid.CreatedAt = DateTime.Now;
                db.Bestuurslid.Add(bestuurslid);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.BestuurslidCategorie, "Id", "Title", bestuurslid.CategoryId);
            adminVM.Bestuurslid = bestuurslid;
            return View(adminVM);
        }

        // GET: Bestuurslid/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Bestuurslid bestuurslid = db.Bestuurslid.Find(id);
            if (bestuurslid == null)
            {
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.BestuurslidCategorie, "Id", "Title", bestuurslid.CategoryId);

            adminVM.Bestuurslid = bestuurslid;
            return View(adminVM);
        }

        // POST: Bestuurslid/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,IsActive,Function,EmailAddress,PhoneNumber,CategoryId,LastModifiedAt,CreatedAt")] Bestuurslid bestuurslid, int CategoryId)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }
            bestuurslid.CategoryId = CategoryId;

            if (ModelState.IsValid)
            {
                bestuurslid.LastModifiedAt = DateTime.Now;
                db.Entry(bestuurslid).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.BestuurslidCategorie, "Id", "Title", bestuurslid.CategoryId);
            adminVM.Bestuurslid = bestuurslid;
            return View(bestuurslid);
        }

        // GET: Bestuurslid/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Bestuurslid bestuurslid = db.Bestuurslid.Find(id);
            if (bestuurslid == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Bestuurslid = bestuurslid;
            return View(adminVM);
        }

        // POST: Bestuurslid/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            Bestuurslid bestuurslid = db.Bestuurslid.Find(id);
            db.Bestuurslid.Remove(bestuurslid);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
