﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;

namespace CVO.Controllers
{
    public class AlbumController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public AlbumController()
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var albumLevel = db.Levels.Find(1).MinLevel;

                //Check if page level not is higher than user level
                if (albumLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: Album
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Get all albums, ordered by dateCreated DESC
            var albums = from a in db.Albums
                         orderby a.DateCreated descending
                         select a;

            adminVM.AlbumList = albums.ToList();

            return View(adminVM);
        }

        // GET: Album/Details/5
        public ActionResult Details(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Album = album;

            return View(adminVM);
        }

        // GET: Album/Create
        public ActionResult Create()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Create object of album
            Album album = new Album();

            //Set active to true, so active checkbox is checked by default
            album.Active = true;

            //Set DateCreated to DateTime.Now
            album.DateCreated = DateTime.Now;

            adminVM.Album = album;

            return View(adminVM);
        }

        // POST: Album/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Active,Image,Url,DateCreated")] Album album)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (album.Image != null)
            {
                //Save image to server and change name to imagepath
                album.Image = Image.SaveImageToServer();
            }

            if (ModelState.IsValid)
            {
                db.Albums.Add(album);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Album = album;

            return View(adminVM);
        }

        // GET: Album/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Album = album;

            return View(adminVM);
        }

        // POST: Album/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Active,Image,Url,DateCreated")] Album album)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (ModelState.IsValid)
            {
                db.Entry(album).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Album = album;

            return View(adminVM);
        }

        // GET: Album/EditImage/5
        public ActionResult EditImage(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Album = album;

            return View(adminVM);
        }

        // POST: Album/EditImage/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditImage([Bind(Include = "Id,Title,Active,Image,Url,DateCreated")] Album album)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (album.Image != null)
            {
                //Save image to server and change name to imagepath
                album.Image = Image.SaveImageToServer();
            }
            else
            {
                return RedirectToAction("Edit", new { id = album.Id });
            }
            if (ModelState.IsValid)
            {
                db.Entry(album).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = album.Id });
            }
            return RedirectToAction("Edit", new { id = album.Id });
        }

        // GET: Album/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Album = album;

            return View(adminVM);
        }

        // POST: Album/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            Album album = db.Albums.Find(id);

            //If image is set, delete image from server
            if (album.Image != null)
            {
                //Delete image
                Image.DeleteImageFromServer(album.Image);
            }

            db.Albums.Remove(album);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
