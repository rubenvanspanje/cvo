﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;

namespace CVO.Controllers
{
    public class NieuwsController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public NieuwsController()
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var newsLevel = db.Levels.Find(7).MinLevel;

                //Check if page level not is higher than user level
                if (newsLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: Nieuws
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            var nieuws = from k in db.Nieuws
                         orderby k.DateCreated descending
                         select k;

            adminVM.NieuwsList = nieuws.ToList();

            return View(adminVM);
        }

        // GET: Nieuws/Details/5
        public ActionResult Details(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Nieuws nieuws = db.Nieuws.Find(id);
            if (nieuws == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Nieuws = nieuws;

            return View(adminVM);
        }

        // GET: Nieuws/Create
        public ActionResult Create()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Create object of Nieuws
            Nieuws nieuws = new Nieuws();

            //Set active to true, so active checkbox is checked by default
            nieuws.Active = true;

            //Set DateCreated to DateTime.Now
            nieuws.DateCreated = DateTime.Now;

            adminVM.Nieuws = nieuws;

            return View(adminVM);
        }

        // POST: Nieuws/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Content,Active,Locked,Image,DateCreated")] Nieuws nieuws)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (nieuws.Image != null)
            {
                //Save image to server and change name to imagepath
                nieuws.Image = Image.SaveImageToServer();
            }

            if (ModelState.IsValid)
            {
                db.Nieuws.Add(nieuws);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Nieuws = nieuws;

            return View(adminVM);
        }

        // GET: Nieuws/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Nieuws nieuws = db.Nieuws.Find(id);
            if (nieuws == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Nieuws = nieuws;

            return View(adminVM);
        }

        // POST: Nieuws/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Content,Active,Locked,Image,DateCreated")] Nieuws nieuws)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (ModelState.IsValid)
            {
                db.Entry(nieuws).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Nieuws = nieuws;

            return View(adminVM);
        }

        // GET: Nieuws/EditImage/5
        public ActionResult EditImage(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Nieuws nieuws = db.Nieuws.Find(id);
            if (nieuws == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Nieuws = nieuws;

            return View(adminVM);
        }

        // POST: Nieuws/EditImage/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditImage([Bind(Include = "Id,Title,Content,Active,Locked,Image,DateCreated")] Nieuws nieuws)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (nieuws.Image != null)
            {
                //Save image to server and change name to imagepath
                nieuws.Image = Image.SaveImageToServer();
            }
            else
            {
                return RedirectToAction("Edit", new { id = nieuws.Id });
            }

            if (ModelState.IsValid)
            {
                db.Entry(nieuws).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = nieuws.Id });
            }
            return RedirectToAction("Edit", new { id = nieuws.Id });
        }

        // POST: Nieuws/DeleteImage/5
        [HttpPost, ActionName("DeleteImage")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteImage([Bind(Include = "Id,Title,Content,Active,Locked,Image,DateCreated")] Nieuws nieuws)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (nieuws.Image != null)
            {
                //Delete image
                Image.DeleteImageFromServer(nieuws.Image);

                //Set image to null in database
                nieuws.Image = null;
            }

            if (ModelState.IsValid)
            {
                db.Entry(nieuws).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Nieuws = nieuws;

            return View(adminVM);
        }

        // GET: Nieuws/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Nieuws nieuws = db.Nieuws.Find(id);
            if (nieuws == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Nieuws = nieuws;

            return View(adminVM);
        }

        // POST: Nieuws/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            Nieuws nieuws = db.Nieuws.Find(id);

            //If image is set, delete image from server
            if (nieuws.Image != null)
            {
                //Delete image
                Image.DeleteImageFromServer(nieuws.Image);
            }

            db.Nieuws.Remove(nieuws);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
