﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;

namespace CVO.Controllers
{
    public class KalenderController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public KalenderController() 
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var calendarLevel = db.Levels.Find(6).MinLevel;

                //Check if page level not is higher than user level
                if (calendarLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: Kalender
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            var kalender = from k in db.Kalender
                           orderby k.DateCreated descending
                           select k;

            adminVM.KalenderList = kalender.ToList();

            return View(adminVM);
        }

        // GET: Kalender/Details/5
        public ActionResult Details(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Kalender kalender = db.Kalender.Find(id);
            if (kalender == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Kalender = kalender;

            return View(adminVM);
        }

        // GET: Kalender/Create
        public ActionResult Create()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Create object of Kalender so View can use GetInputDateValue method
            Kalender kalender = new Kalender();

            //Set active to true, so active checkbox is checked by default
            kalender.Active = true;

            //Set DateCreated to DateTime.Now
            kalender.DateCreated = DateTime.Now;

            adminVM.Kalender = kalender;

            return View(adminVM);
        }

        // POST: Kalender/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,DateStart,DateEnd,Description,Location,Active,Image,DateCreated")] Kalender kalender)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Check if kalender dateStart is not bigger than dateEnd
            if (kalender.DateStart <= kalender.DateEnd)
            {
                //Check if kalender dateStart not is in the past
                if (kalender.DateStart >= DateTime.Now.Date)
                {

                    if (kalender.Image != null)
                    {
                        //Save image to server and change name to imagepath
                        kalender.Image = Image.SaveImageToServer();
                    }

                    if (ModelState.IsValid)
                    {
                        db.Kalender.Add(kalender);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }

            adminVM.Kalender = kalender;

            return View(adminVM);
        }

        // GET: Kalender/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Kalender kalender = db.Kalender.Find(id);
            if (kalender == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Kalender = kalender;

            return View(adminVM);
        }

        // POST: Kalender/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,DateStart,DateEnd,Description,Location,Active,Image,DateCreated")] Kalender kalender)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Check if kalender dateStart is not bigger than dateEnd
            if (kalender.DateStart <= kalender.DateEnd)
            {
                //Check if kalender dateStart not is in the past
                if (kalender.DateStart >= DateTime.Now.Date)
                {
                    if (ModelState.IsValid)
                    {
                        db.Entry(kalender).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }

            adminVM.Kalender = kalender;

            return View(adminVM);
        }

        // GET: Kalender/EditImage/5
        public ActionResult EditImage(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Kalender kalender = db.Kalender.Find(id);
            if (kalender == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Kalender = kalender;

            return View(adminVM);
        }

        // POST: Kalender/EditImage/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditImage([Bind(Include = "Id,Title,DateStart,DateEnd,Description,Location,Active,Image,DateCreated")] Kalender kalender)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (kalender.Image != null)
            {
                //Save image to server and change name to imagepath
                kalender.Image = Image.SaveImageToServer();
            }
            else
            {
                return RedirectToAction("Edit", new { id = kalender.Id });
            }
            if (ModelState.IsValid)
            {
                db.Entry(kalender).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = kalender.Id });
            }
            return RedirectToAction("Edit", new { id = kalender.Id });
        }

        // POST: Kalender/DeleteImage/5
        [HttpPost, ActionName("DeleteImage")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteImage([Bind(Include = "Id,Title,DateStart,DateEnd,Description,Location,Active,Image,DateCreated")] Kalender kalender)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (kalender.Image != null)
            {
                //Delete image
                Image.DeleteImageFromServer(kalender.Image);

                //Set image to null in database
                kalender.Image = null;
            }

            if (ModelState.IsValid)
            {
                db.Entry(kalender).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        // GET: Kalender/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Kalender kalender = db.Kalender.Find(id);
            if (kalender == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Kalender = kalender;

            return View(adminVM);
        }

        // POST: Kalender/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            Kalender kalender = db.Kalender.Find(id);

            //If image is set, delete image from server
            if (kalender.Image != null)
            {
                //Delete image
                Image.DeleteImageFromServer(kalender.Image);
            }

            db.Kalender.Remove(kalender);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
