﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;

namespace CVO.Controllers
{
    public class LevelController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public LevelController()
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var userLevel = db.Levels.Find(4).MinLevel;

                //Check if page level not is higher than user level
                if (userLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: Level/Index
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            var levels = from l in db.Levels
                         orderby l.Category ascending
                         select l;

            adminVM.LevelList = levels.ToList();

            return View(adminVM);
        }

        // GET: Level/EditLevel
        public ActionResult EditLevel(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }

            Level level = db.Levels.Find(id);

            if (level == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Level = level;

            return View(adminVM);
        }

        // POST: Level/EditLevel
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditLevel([Bind(Include = "Id,Category,MinLevel,LastModifiedAt")] Level level)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (level.MinLevel < 1 && level.MinLevel > 3)
            {
                adminVM.Level = level;
                return View(adminVM);
            }

            if (ModelState.IsValid)
            {
                level.LastModifiedAt = DateTime.Now;
                //Save changes in database
                db.Entry(level).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
    }
}