﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;

namespace CVO.Controllers
{
    public class SponsorController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private bool hasPermission;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private string redirectActionForPermission;
        private string redirectControllerForPermission;
        private AdminVM adminVM;

        public SponsorController()
        {
            isLoggedIn = false;
            hasPermission = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            redirectActionForPermission = "Index";
            redirectControllerForPermission = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                //Get level of page from database
                var sponsorLevel = db.Levels.Find(8).MinLevel;

                //Check if page level not is higher than user level
                if (sponsorLevel <= Gebruiker.ReturnCurrentUser().Level)
                {
                    hasPermission = true;
                }

                adminVM = new AdminVM();
            }
        }

        // GET: Sponsor
        public ActionResult Index()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Get all sponsors, ordered by dateCreated desc
            var sponsors = from k in db.Sponsors
                           orderby k.DateCreated descending
                           select k;

            adminVM.SponsorList = sponsors.ToList();

            return View(adminVM);
        }

        // GET: Sponsor/Details/5
        public ActionResult Details(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Sponsor sponsor = db.Sponsors.Find(id);
            if (sponsor == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Sponsor = sponsor;

            return View(adminVM);
        }

        // GET: Sponsor/Create
        public ActionResult Create()
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            //Create object of Sponsor
            Sponsor sponsor = new Sponsor();

            //Set active to true, so active checkbox is checked by default
            sponsor.Active = true;

            //Set DateCreated to DateTime.Now
            sponsor.DateCreated = DateTime.Now;

            adminVM.Sponsor = sponsor;

            return View(adminVM);
        }

        // POST: Sponsor/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Active,Website,Image,DateCreated")] Sponsor sponsor)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (sponsor.Website != null)
            {
                if (!sponsor.Website.Contains("http://") && !sponsor.Website.Contains("https://"))
                {
                    sponsor.Website = "http://" + sponsor.Website;
                }
            }

            if (sponsor.Image != null)
            {
                //Save image to server and change name to imagepath
                sponsor.Image = Image.SaveImageToServer();
            }

            if (ModelState.IsValid)
            {
                db.Sponsors.Add(sponsor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Sponsor = sponsor;

            return View(adminVM);
        }

        // GET: Sponsor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Sponsor sponsor = db.Sponsors.Find(id);
            if (sponsor == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Sponsor = sponsor;

            return View(adminVM);
        }

        // POST: Sponsor/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Active,Website,Image,DateCreated")] Sponsor sponsor)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (sponsor.Website != null)
            {
                if (!sponsor.Website.Contains("http://") && !sponsor.Website.Contains("https://"))
                {
                    sponsor.Website = "http://" + sponsor.Website;
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(sponsor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            adminVM.Sponsor = sponsor;

            return View(adminVM);
        }

        // GET: Sponsor/EditImage/5
        public ActionResult EditImage(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Sponsor sponsor = db.Sponsors.Find(id);
            if (sponsor == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Sponsor = sponsor;

            return View(adminVM);
        }

        // POST: Sponsor/EditImage/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditImage([Bind(Include = "Id,Name,Active,Website,Image,DateCreated")] Sponsor sponsor)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (sponsor.Image != null)
            {
                //Save image to server and change name to imagepath
                sponsor.Image = Image.SaveImageToServer();
            }
            else
            {
                return RedirectToAction("Edit", new { id = sponsor.Id });
            }
            if (ModelState.IsValid)
            {
                db.Entry(sponsor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = sponsor.Id });
            }
            return RedirectToAction("Edit", new { id = sponsor.Id });
        }

        // GET: Sponsor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Sponsor sponsor = db.Sponsors.Find(id);
            if (sponsor == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Sponsor = sponsor;

            return View(adminVM);
        }

        // POST: Sponsor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            else if (!hasPermission)
            {
                return RedirectToAction(redirectActionForPermission, redirectControllerForPermission, null);
            }

            Sponsor sponsor = db.Sponsors.Find(id);

            //If image is set, delete image from server
            if (sponsor.Image != null)
            {
                //Delete image
                Image.DeleteImageFromServer(sponsor.Image);
            }

            db.Sponsors.Remove(sponsor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
