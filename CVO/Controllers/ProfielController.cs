﻿using CVO.DAL;
using CVO.Models;
using CVO.ViewModels;
using System;
using System.Data.Entity;
using System.Web.Mvc;

namespace CVO.Controllers
{
    public class ProfielController : Controller
    {
        private CvoDbContext db = new CvoDbContext();
        private bool isLoggedIn;
        private string redirectActionForLogin;
        private string redirectControllerForLogin;
        private AdminVM adminVM;

        public ProfielController()
        {
            isLoggedIn = false;

            redirectActionForLogin = "Login";
            redirectControllerForLogin = "Admin";

            //Check if user is logged in
            if (Gebruiker.CheckIfLoggedIn())
            {
                isLoggedIn = true;

                adminVM = new AdminVM();
            }
        }

        // GET: Profiel/ChangePassword/5
        public ActionResult ChangePassword(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            //If id not is the id of the current user
            //Redirect to Index
            if (id != Gebruiker.ReturnCurrentUser().Id)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            ChangePassword changePassword = new ChangePassword();
            changePassword.Id = (int)id;

            adminVM.ChangePassword = changePassword;

            return View(adminVM);
        }

        // POST: Profiel/ChangePassword/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword([Bind(Include = "Id,CurrentPassword,NewPassword,NewPasswordRepeat")] ChangePassword changePassword)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }

            //RubenTODO: Functionaliteit in model

            //Check if form input is valid
            if (ModelState.IsValid)
            {
                //Get current user from the database
                Gebruiker gebruiker = db.Gebruikers.Find(changePassword.Id);

                var errorMessage = changePassword.ChechIfPasswordMeetsRequirements(changePassword, gebruiker);

                if (errorMessage == null)
                {
                    string encryptedNewPassword = Gebruiker.EncryptPassword(changePassword.NewPassword);

                    //Set password in database to new encrypted password
                    gebruiker.Password = encryptedNewPassword;
                    gebruiker.LastModifiedAt = DateTime.Now;

                    db.Entry(gebruiker).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Edit", new { id = gebruiker.Id });
                }
                else
                {
                    ViewBag.errorMessage = errorMessage;
                    return View(adminVM);
                }
            }
            else
            {
                return View(adminVM);
            }
        }

        // GET: Profiel/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }

            if (id == null)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            //If id not is the id of the current user
            //Redirect to Index
            if (id != Gebruiker.ReturnCurrentUser().Id)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            Gebruiker gebruiker = db.Gebruikers.Find(id);

            if (gebruiker == null)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            adminVM.Gebruiker = gebruiker;

            return View(adminVM);
        }

        // POST: Profiel/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Email,Image,Password,Level,IsBlocked,LastModifiedAt,CreatedAt,MainAccount")] Gebruiker gebruiker)
        {

            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }

            //If id not is the id of the current user
            //Redirect to Index
            if (gebruiker.Id != Gebruiker.ReturnCurrentUser().Id)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            //RubenTODO: Controleren of email al bestaat in de database

            if (ModelState.IsValid)
            {
                gebruiker.LastModifiedAt = DateTime.Now;
                //Save changes in database
                db.Entry(gebruiker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Admin", null);
            }

            return RedirectToAction("Index", "Admin", null);
        }

        // GET: Gebruiker/EditImage/5
        public ActionResult EditImage(int? id)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            //If id not is the id of the current user
            //Redirect to Index
            if (id != Gebruiker.ReturnCurrentUser().Id)
            {
                return RedirectToAction("Index", "Admin", null);
            }


            if (id == null)
            {
                return RedirectToAction("Index");
            }
            Gebruiker gebruiker = db.Gebruikers.Find(id);
            if (gebruiker == null)
            {
                return RedirectToAction("Index");
            }

            adminVM.Gebruiker = gebruiker;

            return View(adminVM);
        }

        // POST: Gebruiker/EditImage/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditImage([Bind(Include = "Id,FirstName,LastName,Email,Image,Password,Level,IsBlocked,LastModifiedAt,CreatedAt,MainAccount")] Gebruiker gebruiker)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }
            //If id not is the id of the current user
            //Redirect to Index
            if (gebruiker.Id != Gebruiker.ReturnCurrentUser().Id)
            {
                return RedirectToAction("Index", "Admin", null);
            }


            if (gebruiker.Image != null)
            {
                //Save image to server and change name to imagepath
                gebruiker.Image = Image.SaveImageToServer();
            }
            else
            {
                return RedirectToAction("Edit", new { id = gebruiker.Id });
            }
            if (ModelState.IsValid)
            {
                gebruiker.LastModifiedAt = DateTime.Now;
                db.Entry(gebruiker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = gebruiker.Id });
            }
            return RedirectToAction("Edit", new { id = gebruiker.Id });
        }

        // POST: Profiel/DeleteImage
        [HttpPost, ActionName("DeleteImage")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteImage([Bind(Include = "Id,FirstName,LastName,Email,Image,Password,Level,IsBlocked,LastModifiedAt,CreatedAt,MainAccount")] Gebruiker gebruiker)
        {
            if (!isLoggedIn)
            {
                return RedirectToAction(redirectActionForLogin, redirectControllerForLogin, null);
            }

            //If id not is the id of the current user
            //Redirect to Index
            if (gebruiker.Id != Gebruiker.ReturnCurrentUser().Id)
            {
                return RedirectToAction("Index", "Admin", null);
            }

            if (gebruiker.Image != null)
            {
                //Delete image
                Image.DeleteImageFromServer(gebruiker.Image);

                //Set image to null in database
                gebruiker.Image = null;
            }

            if (ModelState.IsValid)
            {
                db.Entry(gebruiker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit");
            }

            return RedirectToAction("Edit");
        }
    }
}