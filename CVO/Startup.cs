﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CVO.Startup))]
namespace CVO
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
