﻿using System.Web.Mvc;
using System.Web.Routing;

namespace CVO
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Search",
                url: "{controller}/{action}/{search}/{pageNieuws}/{pageTeam}/{pageSponsor}/{pageKalender}/{pageAlbum}",
                defaults: new
                {
                    controller = "Home",
                    action = "Zoeken",
                    search = UrlParameter.Optional,
                    pageNieuws = UrlParameter.Optional,
                    pageTeam = UrlParameter.Optional,
                    pageSponsor = UrlParameter.Optional,
                    pageKalender = UrlParameter.Optional,
                    pageAlbum = UrlParameter.Optional
                }
            );
        }
    }
}
