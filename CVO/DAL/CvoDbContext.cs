﻿using System.Data.Entity;
using CVO.Models;

namespace CVO.DAL
{
    public class CvoDbContext : DbContext
    {
        public DbSet<Nieuws> Nieuws { get; set; }
        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<Kalender> Kalender { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Locatie> Locatie { get; set; }
        public DbSet<Gebruiker> Gebruikers { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Bestuurslid> Bestuurslid { get; set; }
        public DbSet<BestuurslidCategorie> BestuurslidCategorie { get; set; }
    }
}