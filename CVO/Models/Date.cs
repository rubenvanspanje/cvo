﻿using System;

namespace CVO.Models
{
    public static class Date
    {
        public static string ReturnDutchNameOfMonth(DateTime datetime)
        {
            if (datetime.Month == 1)
            {
                return "Jan";
            }
            else if (datetime.Month == 2)
            {
                return "Feb";
            }
            else if (datetime.Month == 3)
            {
                return "Maa";
            }
            else if (datetime.Month == 4)
            {
                return "Apr";
            }
            else if (datetime.Month == 5)
            {
                return "Mei";
            }
            else if (datetime.Month == 6)
            {
                return "Jun";
            }
            else if (datetime.Month == 7)
            {
                return "Jul";
            }
            else if (datetime.Month == 8)
            {
                return "Aug";
            }
            else if (datetime.Month == 9)
            {
                return "Sep";
            }
            else if (datetime.Month == 10)
            {
                return "Okt";
            }
            else if (datetime.Month == 11)
            {
                return "Nov";
            }
            else
            {
                return "Dec";
            }
        }

        public static string ShowDateOfDateTime(DateTime item)
        {
            string date = item.Day + "-" + item.Month + "-" + item.Year;

            return date;
        }

        public static string ShowDateOfDateTimeWithMonthName(DateTime item)
        {
            string date = item.Day + " " + Date.ReturnDutchNameOfMonth(item) + " " + item.Year;

            return date;
        }
    }
}