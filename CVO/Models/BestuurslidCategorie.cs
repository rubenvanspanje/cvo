﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CVO.Models
{
    public class BestuurslidCategorie
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Titel is verplicht!")]
        [DataType(DataType.Text)]
        [DisplayName("Titel")]
        public string Title { get; set; }

        [DisplayName("Calamiteit")]
        public bool IsCalamity { get; set; }

        [Required]
        [DisplayName("Laatst gewijzigd op")]
        public DateTime LastModifiedAt { get; set; }

        [Required]
        [DisplayName("Toegevoegd op")]
        public DateTime CreatedAt { get; set; }
    }
}