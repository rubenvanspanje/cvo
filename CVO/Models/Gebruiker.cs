﻿using CVO.DAL;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CVO.Models
{
    public class Gebruiker
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Voornaam is verplicht!")]
        [DataType(DataType.Text)]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Achternaam is verplicht!")]
        [DataType(DataType.Text)]
        [DisplayName("Achternaam")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is verplicht!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Wachtwoord is verplicht!")]
        [DataType(DataType.Password)]
        [DisplayName("Wachtwoord")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Niveau is verplicht!")]
        [DisplayName("Niveau")]
        public int Level { get; set; }

        [Required]
        [DisplayName("Hoofdaccount")]
        public bool MainAccount { get; set; }

        [DataType(DataType.Upload)]
        [DisplayName("Profielfoto")]
        public string Image { get; set; }

        [Required]
        [DisplayName("Geblokkeerd")]
        public bool IsBlocked { get; set; }

        [Required]
        [DisplayName("Laatst gewijzigd op")]
        public DateTime LastModifiedAt { get; set; }

        [Required]
        [DisplayName("Toegevoegd op")]
        public DateTime CreatedAt { get; set; }

        public static string EncryptPassword(string inputString)
        {
            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha512.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        public static bool CheckIfLoggedIn()
        {
            if (HttpContext.Current.Session["userId"] == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static Gebruiker ReturnCurrentUser()
        {
            //userId = id of session
            int userId = Convert.ToInt32(HttpContext.Current.Session["userId"]);

            //Search for user by id in database
            CvoDbContext db = new CvoDbContext();
            Gebruiker user = db.Gebruikers.Find(userId);

            return user;
        }

        public HtmlString CheckIfUserIsBlocked(bool IsBlocked)
        {
            HtmlString htmlstring;

            if(IsBlocked)
            {
                htmlstring = new HtmlString("<i class='fa fa-check'></i>");
            } else
            {
                htmlstring = new HtmlString("<i class='fa fa-times'></i>");
            }
            

            return htmlstring;
        }
    }
}