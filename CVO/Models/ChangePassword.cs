﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace CVO.Models
{
    public class ChangePassword
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Huidig wachtwoord is vereist")]
        [DataType(DataType.Password)]
        [DisplayName("Huidig wachtwoord")]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "Nieuw wachtwoord is vereist")]
        [DataType(DataType.Password)]
        [DisplayName("Nieuw wachtwoord")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Nieuw wachtwoord herhaling is vereist")]
        [DataType(DataType.Password)]
        [DisplayName("Nieuw wachtwoord herhaling")]
        public string NewPasswordRepeat { get; set; }

        public string ChechIfPasswordMeetsRequirements(ChangePassword changePassword, Gebruiker gebruiker)
        {

            //Call gebruiker method to encrypt passwords
            string encryptedCurrentPassword = Gebruiker.EncryptPassword(changePassword.CurrentPassword);
            string encryptedNewPassword = Gebruiker.EncryptPassword(changePassword.NewPassword);
            string encryptedNewPasswordRepeat = Gebruiker.EncryptPassword(changePassword.NewPasswordRepeat);

            //Check if password is the same as database password
            if (encryptedCurrentPassword != gebruiker.Password)
            {
                return "Huidig wachtwoord is onjuist";
            }

            //Regex for special regex
            var specialCharactersRegex = new Regex("^[!,@,#,$,%,^,&,*,?,_,~,-,(,)]*$");

            //Check if new password contains at least 8 characters
            if (changePassword.NewPassword.Length < 8)
            {
                return "Wachtwoord moet minstens 8 karakters bevatten";
            }
            //Check if new password contains a number
            else if (!changePassword.NewPassword.Any(char.IsDigit))
            {
                return "Wachtwoord moet minstens 1 cijfer bevatten";
            }
            //Check if new password contains a capital letter
            else if (!changePassword.NewPassword.Any(char.IsUpper))
            {
                return "Wachtwoord moet minstens 1 hoofdletter bevatten";
            }
            //Check if new password contains a special character
            else if (specialCharactersRegex.IsMatch(changePassword.NewPassword))
            {
                return "Wachtwoord moet minstens 1 speciaal karakter bevatten";
            }

            //Check if newPassword and newPasswordRepeat are the same
            if (encryptedNewPassword != encryptedNewPasswordRepeat)
            {
                return "Wachtwoorden zijn niet hetzelfde";
            }

            return null;
        }
    }
}