﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CVO.Models
{
    public class Team
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Naam is verplicht!")]
        [DisplayName("Team")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Foto is verplicht")]
        [DataType(DataType.Upload)]
        [DisplayName("Foto")]
        public string Image { get; set; }

        [DisplayName("Omschrijving")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Actief")]
        public bool Active { get; set; }

        [DisplayName("Sponsor naam")]
        public string SponsorName { get; set; }

        [DisplayName("Sponsor website")]
        public string SponsorWebsite { get; set; }

        [DisplayName("Sponsor foto")]
        [DataType(DataType.Upload)]
        public string SponsorImage { get; set; }

        [DisplayName("Datum")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
    }
}