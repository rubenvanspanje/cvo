﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVO.Models
{
    public class Level
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Categorie is verplicht!")]
        [DataType(DataType.Text)]
        [DisplayName("Categorie")]
        public string Category { get; set; }

        [Required(ErrorMessage = "Niveau is verplicht!")]
        [DisplayName("Niveau")]
        public int MinLevel { get; set; }

        [Required]
        [DisplayName("Laatst gewijzigd op")]
        public DateTime LastModifiedAt { get; set; }

    }
}