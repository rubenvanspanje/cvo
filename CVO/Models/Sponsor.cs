﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVO.Models
{
    public class Sponsor
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Naam is verplicht!")]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [DisplayName("Actief")]
        public bool Active { get; set; }

        public string Website { get; set; }

        [Required(ErrorMessage = "Foto is verplicht!")]
        [DataType(DataType.Upload)]
        [DisplayName("Foto")]
        public string Image { get; set; }

        [DisplayName("Datum")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }

        public HtmlString addLinkToImage(string website, string image, string name)
        {
            //Check if website not is null
            //Null = show image
            //Not Null = a href to website + img-hover class
            //Type = HtmlString
            HtmlString htmlstring;

            if (website == null)
            {
                htmlstring = new HtmlString("<img src='" + image + "' alt='" + name + "' class='img-responsive img-sponsor img-center' />");
            }
            else
            {
                htmlstring = new HtmlString("<a href='" + website + "' target='_blank'><img src='" + image + "' alt='" + name + "' class='img-responsive img-hover img-sponsor img-center'/></a>");
            }

            return htmlstring;
        }
    }
}