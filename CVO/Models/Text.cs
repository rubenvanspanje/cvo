﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CVO.Models
{
    public static class Text
    {
        public static string GetPartOfString(string content, int maxLength)
        {

            string returnString;

            if (content == null)
            {
                return returnString = "";
            }

            if (content.Length > maxLength)
            {
                returnString = content.Substring(0, maxLength) + "...";
            }
            else
            {
                returnString = content;
            }
            return returnString;
        }
    }
}