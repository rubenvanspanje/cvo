﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace CVO.Models
{
    public class NewUser
    {
        [Required(ErrorMessage = "Voornaam is verplicht!")]
        [DataType(DataType.Text)]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Achternaam is verplicht!")]
        [DataType(DataType.Text)]
        [DisplayName("Achternaam")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is verplicht!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Wachtwoord is verplicht!")]
        [DataType(DataType.Password)]
        [DisplayName("Wachtwoord")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Wachtwoord herhaling is verplicht")]
        [DataType(DataType.Password)]
        [DisplayName("Wachtwoord herhaling")]
        public string PasswordRepeat { get; set; }

        [Required(ErrorMessage = "Niveau is verplicht!")]
        [DisplayName("Niveau")]
        public int Level { get; set; }

        [DataType(DataType.Upload)]
        [DisplayName("Profielfoto")]
        public string Image { get; set; }

        public string ChechIfPasswordMeetsRequirements(NewUser newUser)
        {

            //Call gebruiker method to encrypt passwords
            string encryptedPassword = Gebruiker.EncryptPassword(newUser.Password);
            string encryptedPasswordRepeat = Gebruiker.EncryptPassword(newUser.PasswordRepeat);

            //Regex for special regex
            var specialCharactersRegex = new Regex("^[!,@,#,$,%,^,&,*,?,_,~,-,(,)]*$");

            //Check if new password contains at least 8 characters
            if (newUser.Password.Length < 8)
            {
                return "Wachtwoord moet minstens 8 karakters bevatten";
            }
            //Check if new password contains a number
            else if (!newUser.Password.Any(char.IsDigit))
            {
                return "Wachtwoord moet minstens 1 cijfer bevatten";
            }
            //Check if new password contains a capital letter
            else if (!newUser.Password.Any(char.IsUpper))
            {
                return "Wachtwoord moet minstens 1 hoofdletter bevatten";
            }
            //Check if new password contains a special character
            else if (specialCharactersRegex.IsMatch(newUser.Password))
            {
                return "Wachtwoord moet minstens 1 speciaal karakter bevatten";
            }

            //Check if newPassword and newPasswordRepeat are the same
            if (encryptedPassword != encryptedPasswordRepeat)
            {
                return "Wachtwoorden zijn niet hetzelfde";
            }

            return null;
        }
    }
}