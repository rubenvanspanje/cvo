﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CVO.Models
{
    public class Bestuurslid
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Titel is verplicht!")]
        [DataType(DataType.Text)]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Functie is verplicht!")]
        [DataType(DataType.Text)]
        [DisplayName("Functie")]
        public string Function { get; set; }

        [Required(ErrorMessage = "Email is verplicht!")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Telefoonnummer is verplicht!")]
        [DataType(DataType.PhoneNumber)]
        [DisplayName("Telefoonnummer")]
        public string PhoneNumber { get; set; }

        [DisplayName("Actief")]
        public bool IsActive { get; set; }

        [ForeignKey("Category")]
        [DisplayName("Categorie")]
        [Required(ErrorMessage = "Categorie is verplicht!")]
        public int CategoryId { get; set; }

        public virtual BestuurslidCategorie Category { get; set; }

        [Required]
        [DisplayName("Laatst gewijzigd op")]
        public DateTime LastModifiedAt { get; set; }

        [Required]
        [DisplayName("Toegevoegd op")]
        public DateTime CreatedAt { get; set; }
    }
}