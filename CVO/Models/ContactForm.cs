﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVO.Models
{
    public class ContactForm
    {
        [Required(ErrorMessage = "Naam is verplicht!")]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Telefoonnummer is verplicht!")]
        [DisplayName("Telefoonnummer")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email is verplicht!")]
        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Bericht is verplicht!")]
        [DisplayName("Bericht")]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
    }
}