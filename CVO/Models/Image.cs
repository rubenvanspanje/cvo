﻿using System;
using System.IO;
using System.Web;
using System.Web.Helpers;

namespace CVO.Models
{
    public static class Image
    {

        public static HtmlString ShowImage(string image, bool textCenter)
        {
            //Check if image not is null
            //Null = fa fa-times
            //Not Null = show image
            //Type = HtmlString
            HtmlString htmlstring;

            if (image == null)
            {
                if (textCenter)
                {
                    htmlstring = new HtmlString("<div class='text-center'><i class='fa fa-times'></i></div>");
                }
                else
                {
                    htmlstring = new HtmlString("<i class='fa fa-times'></i>");
                }
            }
            else
            {
                if (textCenter)
                {
                    htmlstring = new HtmlString("<img src='" + image + "' class='img-responsive img-index img-center' />");
                }
                else
                {
                    htmlstring = new HtmlString("<img src='" + image + "' class='img-responsive img-index' />");
                }
            }

            return htmlstring;
        }

        public static string SaveImageToServer()
        {
            //Give image and fileName null value
            WebImage image = null;
            var imagePath = "";

            //Give image the right image
            image = WebImage.GetImageFromRequest();

            //If image not is null
            if (image != null)
            {
                var fileName = "";

                //Create a unique fileName (Guid) and get path of image                
                fileName = Guid.NewGuid().ToString() + "_" + Path.GetFileName(image.FileName);
                imagePath = @"Images\" + fileName;

                //Save image in Images directory
                image.Save(@"~\" + imagePath, "png", false);

                //Change name of image to imagePath + \ (root)
                imagePath = @"\" + imagePath;
            }

            return imagePath;
        }

        public static HtmlString ShowCurrentImage(string image, string title)
        {
            HtmlString htmlstring;

            if (image != null)
            {
                htmlstring = new HtmlString("<img src='" + image + "' alt='" + title + "' class='img-responsive img-small' />");
            }
            else
            {
                htmlstring = new HtmlString("<p><i>Er is nog geen foto toegewezen.</i></p>");
            }

            return htmlstring;
        }

        public static void DeleteImageFromServer(string image)
        {
            // Set filePath to /Images/{filename}
            var filePath = HttpContext.Current.Server.MapPath(@"~\" + image);

            FileInfo file = new FileInfo(filePath);

            //Check if file exists
            if (file.Exists)
            {
                //Delete file from folder
                file.Delete();
            }
        }

        public static HtmlString ShowDefaultProfileImage()
        {
            HtmlString htmlstring;

            htmlstring = new HtmlString("<img src='/Images/user-placeholder.png' alt='Gebruiker placeholder' class='img-responsive img-small img-center' />");

            return htmlstring;
        }

    }
}