﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVO.Models
{
    public class Nieuws
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Titel is verplicht!")]
        [DisplayName("Titel")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Content is verplicht!")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [DisplayName("Actief")]
        public bool Active { get; set; }

        [DisplayName("Vastgezet")]
        public bool Locked { get; set; }

        [DataType(DataType.Upload)]
        [DisplayName("Foto")]
        public string Image { get; set; }

        [DisplayName("Datum")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
    }
}