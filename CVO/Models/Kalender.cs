﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CVO.Models
{
    public class Kalender
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Titel is verplicht!")]
        [DisplayName("Titel")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Startdatum is verplicht")]
        [DataType(DataType.DateTime)]
        [DisplayName("Startdatum")]
        public DateTime DateStart { get; set; }

        [Required(ErrorMessage = "Einddatum is verplicht")]
        [DataType(DataType.DateTime)]
        [DisplayName("Einddatum")]
        public DateTime DateEnd { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Omschrijving")]
        public string Description { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Locatie")]
        public string Location { get; set; }

        [DisplayName("Actief")]
        public bool Active { get; set; }

        [DataType(DataType.Upload)]
        [DisplayName("Foto")]
        public string Image { get; set; }

        [DisplayName("Datum toegevoegd")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }

        public string ShowIndexFormat(DateTime dateStart, DateTime dateEnd)
        {
            string format;
            if (dateStart == dateEnd)
            {
                format = dateStart.Day.ToString();
                format += " " + Date.ReturnDutchNameOfMonth(dateStart);
            }
            else
            {
                format = dateStart.Day.ToString();
                format += " " + Date.ReturnDutchNameOfMonth(dateStart);
                format += " - ";
                format += dateEnd.Day.ToString();
                format += " " + Date.ReturnDutchNameOfMonth(dateEnd);
            }

            return format;
        }

        public string GetInputDateValue(DateTime item)
        {
            //Change a DateTime to input type = date format (yyyy-MM-dd)
            string monthFormat;
            string dayFormat;

            //Make sure month has 2 characters
            if (item.Month < 10)
            {
                monthFormat = "0" + item.Month;
            }
            else
            {
                monthFormat = item.Month.ToString();
            }

            //Make sure day has 2 characters
            if (item.Day < 10)
            {
                dayFormat = "0" + item.Day;
            }
            else
            {
                dayFormat = item.Day.ToString();
            }
            string date = item.Year + "-" + monthFormat + "-" + dayFormat;

            return date;
        }


    }
}