﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CVO.Models
{
    public class Locatie
    {
        public int Id { get; set; }

        [DisplayName("Sportpark")]
        public string Stadium { get; set; }

        [Required(ErrorMessage = "Straat is verplicht!")]
        [DisplayName("Straatnaam")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Nummer is verplicht!")]
        [DisplayName("Nummer")]
        public string Number { get; set; }

        [Required(ErrorMessage = "Postcode is verplicht!")]
        [DisplayName("Postcode")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Stad is verplicht!")]
        [DisplayName("Stad")]
        public string City { get; set; }

        [Required(ErrorMessage = "Telefoonnummer is verplicht!")]
        [DisplayName("Telefoonnummer")]
        public string Phone { get; set; }

    }
}